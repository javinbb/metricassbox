// Java code to implement the approach
 
import java.util.*;
 
class GFG {
 
    int[][] hadamard;


    public GFG( int hadamardSize ) {
        // Computing n = 2^M
        // using pow() method of Math class
        int n = (int)Math.pow(2, hadamardSize);
 
        // Initializing a matrix of order n
        hadamard = new int[n][n];
 
        // Initializing the 0th column and
        // 0th row element as 1
        this.hadamard[0][0] = 1;
        for (int k = 1; k < n; k += k) {
 
            // Loop to copy elements to
            // other quarters of the matrix
            for (int i = 0; i < k; i++) {
                for (int j = 0; j < k; j++) {
                    hadamard[i + k][j]
                        = hadamard[i][j];
                    hadamard[i][j + k]
                        = hadamard[i][j];
                    hadamard[i + k][j + k]
                        = -hadamard[i][j];
                }
            }
        }
    }

    public int[][] getHadamard() {
        return this.hadamard;
    }
    
    // public static void generate(int M)
    // {
    //     // Computing n = 2^M
    //     // using pow() method of Math class
    //     int n = (int)Math.pow(2, M);
 
    //     // Initializing a matrix of order n
    //     int[][] hadamard = new int[n][n];
 
    //     // Initializing the 0th column and
    //     // 0th row element as 1
    //     hadamard[0][0] = 1;
    //     for (int k = 1; k < n; k += k) {
 
    //         // Loop to copy elements to
    //         // other quarters of the matrix
    //         for (int i = 0; i < k; i++) {
    //             for (int j = 0; j < k; j++) {
    //                 hadamard[i + k][j]
    //                     = hadamard[i][j];
    //                 hadamard[i][j + k]
    //                     = hadamard[i][j];
    //                 hadamard[i + k][j + k]
    //                     = -hadamard[i][j];
    //             }
    //         }
    //     }
 
    //     // Displaying the final hadamard matrix
    //     for (int i = 0; i < n; i++) {
    //         for (int j = 0; j < n; j++) {
    //             if (hadamard[i][j]<1)
    //                 System.out.print(" "+ hadamard[i][j]);
    //             else
    //                 System.out.print("  "+hadamard[i][j]);
    //         }
    //         System.out.println();
    //         System.out.println();
    //     }
    // }
 
    // // Driver code
    // public static void main(String[] args)
    // {
    //     int M = 3;
 
    //     // Function call
    //     generate(M);
    // }
}