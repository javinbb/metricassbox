public class NoLinealityAes {
    
    public static void main(String[] args)
    {
        AES aes = new AES();

        Short[][] tablaDeVerdad = aes.getSboxTruthTable(Byte.parseByte("1"));

        int row = tablaDeVerdad.length;
        int col = tablaDeVerdad[0].length;

        int [] vectorHammingDES = {0, 0, 0, 0, 0, 0, 0, 0};

        for (int i=0 ; i < row; i++)
        {
            System.out.println();
            for (int j=0; j<col;j++ )
            {
                System.out.print( tablaDeVerdad[i][j] + " " );
            }
            System.out.print("Linea " + i);   
        }
        System.out.println();

        for (int i=0 ; i < row; i++)
        {
            if (tablaDeVerdad[i][0]!=tablaDeVerdad[i][8]){
                vectorHammingDES[0] = vectorHammingDES[0] + 1;
            }
            if (tablaDeVerdad[i][1]!=tablaDeVerdad[i][8]){
                vectorHammingDES[1] = vectorHammingDES[1] + 1;
            }
            if (tablaDeVerdad[i][2]!=tablaDeVerdad[i][8]){
                vectorHammingDES[2] = vectorHammingDES[2] + 1;
            }
            if (tablaDeVerdad[i][3]!=tablaDeVerdad[i][8]){
                vectorHammingDES[3] = vectorHammingDES[3] + 1;
            }
            if (tablaDeVerdad[i][4]!=tablaDeVerdad[i][8]){
                vectorHammingDES[4] = vectorHammingDES[4] + 1;
            }
            if (tablaDeVerdad[i][5]!=tablaDeVerdad[i][8]){
                vectorHammingDES[5]=vectorHammingDES[5] + 1;
            }
            if (tablaDeVerdad[i][6]!=tablaDeVerdad[i][8]){
                vectorHammingDES[6]=vectorHammingDES[6] + 1;
            }
            if (tablaDeVerdad[i][7]!=tablaDeVerdad[i][8]){
                vectorHammingDES[7]=vectorHammingDES[7] + 1;
            }
        }
        System.out.println();

        for (int i=0;i<8;i++){
            System.out.print(" " + i + " " + vectorHammingDES[i] + " ");
        }

        for (int i=0;i<8;i++){
            vectorHammingDES[i]=0;
        }

        for (int i=0 ; i < row; i++)
        {
            if (tablaDeVerdad[i][0]!=tablaDeVerdad[i][9]){
                vectorHammingDES[0] = vectorHammingDES[0] + 1;
            }
            if (tablaDeVerdad[i][1]!=tablaDeVerdad[i][9]){
                vectorHammingDES[1] = vectorHammingDES[1] + 1;
            }
            if (tablaDeVerdad[i][2]!=tablaDeVerdad[i][9]){
                vectorHammingDES[2] = vectorHammingDES[2] + 1;
            }
            if (tablaDeVerdad[i][3]!=tablaDeVerdad[i][9]){
                vectorHammingDES[3] = vectorHammingDES[3] + 1;
            }
            if (tablaDeVerdad[i][4]!=tablaDeVerdad[i][9]){
                vectorHammingDES[4] = vectorHammingDES[4] + 1;
            }
            if (tablaDeVerdad[i][5]!=tablaDeVerdad[i][9]){
                vectorHammingDES[5]=vectorHammingDES[5] + 1;
            }
            if (tablaDeVerdad[i][6]!=tablaDeVerdad[i][9]){
                vectorHammingDES[6]=vectorHammingDES[6] + 1;
            }
            if (tablaDeVerdad[i][7]!=tablaDeVerdad[i][9]){
                vectorHammingDES[7]=vectorHammingDES[7] + 1;
            }
        }
        System.out.println();

        for (int i=0;i<8;i++){
            System.out.print(" " + i + " " + vectorHammingDES[i] + " ");
        }


    }

}
