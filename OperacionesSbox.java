import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class OperacionesSbox {
    public static void main(String[] args)
    {
        int fila=0;
        int columna=0;

        Double miu=3.999999;
        Double b=0.499999;
        Short []vectorSbox= new Short[256];
        Boolean []vectorMarcas= new Boolean[256];
        double randomDbl;

        Short elementoArchivo= new Short("0");
        short contenidoSbox;
        short [][] sboxArchivo = new short[16][16];
        Sbox sbox= new Sbox();

        //sbox.setSbox(sbox.gwo1);

        //Short[][] tablaDeVerdad = sbox.getSboxTruthTable();
 
        //int row = tablaDeVerdad.length;
        //int col = tablaDeVerdad[0].length;

        //System.out.print("Fila " + (row));
        //System.out.print("Columna " + (col));

        /*for (int i=0 ; i < row; i++)
        {
            System.out.println();
            for (int j=8; j<col;j++ )
            {
                System.out.print( tablaDeVerdad[i][j] + " " );
            }
            System.out.print("Linea " + (i));   
        }*/
        //System.out.println();

        //sbox.getSboxNonLineality();

        //sbox.getStrictAvalancheCriterion();

        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        //System.out.println("Working Directory = " + System.getProperty("user.dir"));

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File (System.getProperty("user.dir")+"\\"+"matriz.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String SPACE = " ";
            String [] numeros = new String[8];
            String linea;
            String cadena ;
            //System.out.print(" Tamanho "+numeros.length);
            System.out.println("");
            while((linea=br.readLine())!=null){

                //System.out.println(linea);
                numeros = linea.split(SPACE);
                
                for (int i =0; i<numeros.length ;i++){
                    
                    cadena = String.format("%03d" , Integer.parseInt(numeros[i]));
                    //System.out.print(" Tamanho "+numeros.length);
                    System.out.print(" "+cadena);
                    sboxArchivo[fila][i]= Short.parseShort(numeros[i]);

                }

                fila++;
                System.out.println();
            }
                
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try{                    
                if( null != fr ){   
                    fr.close();     
                }                  
            }catch (Exception e2){ 
                e2.printStackTrace();
            }
        }
        System.out.println();

        sbox.setSbox(sboxArchivo);

        sbox.getSboxTruthTable();
        sbox.printSboxTruthTable();


        sbox.getSboxNonLineality();

        sbox.getStrictAvalancheCriterion();


        System.out.println();

        for (int i=0;i<256;i++){
            vectorMarcas[i]=false;
        }

        /*System.out.println("SBOX 1");
        for (int i=0;i<256;i++){
            randomDbl = Math.random();
            //randomDbl = 0.45;
            do {
                for (int j=0;j<13;j++){
                    randomDbl = randomDbl * miu * (1 - randomDbl);
                }

                for (int j=0;j<7;j++){
                    if (randomDbl<= b){
                        randomDbl = randomDbl/b;
                    }else{
                        randomDbl = (1-randomDbl)/(1-b);
                    }
                }
                contenidoSbox = (short)Math.floor(randomDbl*256);

            } while (vectorMarcas[contenidoSbox]);
            vectorSbox[i]=contenidoSbox;
            vectorMarcas[contenidoSbox]=true;

            System.out.print(contenidoSbox+" ");
            if (i % 16==0 && i!=0 ){
                System.out.println();
            }

        }

        System.out.println();
        System.out.println("SBOX 2");

        for (int i=0;i<256;i++){
            vectorMarcas[i]=false;
        }
        
        for (int i=0;i<256;i++){
            randomDbl = Math.random();
            //randomDbl = 0.45;
            do {
                for (int j=0;j<13;j++){
                    randomDbl = randomDbl * miu * (1 - randomDbl);
                }

                for (int j=0;j<7;j++){
                    if (randomDbl<= b){
                        randomDbl = randomDbl/b;
                    }else{
                        randomDbl = (1-randomDbl)/(1-b);
                    }
                }
                contenidoSbox = (short)Math.floor(randomDbl*256);

            } while (vectorMarcas[contenidoSbox]);
            vectorSbox[i]=contenidoSbox;
            vectorMarcas[contenidoSbox]=true;

            System.out.print(contenidoSbox+" ");
            if (i % 16==0 && i!=0 ){
                System.out.println();
            }

        }*/



    }
}
