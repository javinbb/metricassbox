import java.math.BigInteger;
import java.text.DecimalFormat;

import javax.lang.model.util.ElementScanner6;

public class Sbox {

    short  [][] gwo1 = {{51,121,74,104,99,163,90,102,72,78,142,122,219,129,183,155},
    {156,73,54,35,12,79,133,134,128,175,48,86,50,59,232,138},
    {101,118,181,9,110,189,65,32,143,217,88,70,44,107,190,21},
    {214,160,152,19,22,201,49,244,117,96,157,168,16,167,182,191},
    {150,18,114,123,66,172,131,0,137,55,112,140,80,174,53,151},
    {61,136,141,207,203,248,28,77,246,221,177,4,210,149,147,199},
    {81,68,132,71,236,229,158,63,254,218,231,52,76,23,180,108},
    {8,247,20,209,242,196,115,34,67,230,161,87,241,200,192,84},
    {124,173,26,127,14,227,185,249,11,146,233,17,220,105,223,25},
    {253,206,198,46,224,93,56,166,130,97,2,135,237,116,202,171},
    {213,40,176,62,109,226,6,139,243,205,27,252,165,33,238,69},
    {24,95,215,188,85,30,187,212,100,94,125,41,204,159,193,113},
    {197,222,194,211,154,119,64,184,13,43,31,57,164,144,89,92},
    {153,5,39,91,145,178,186,170,45,111,15,245,75,179,126,169},
    {235,120,234,148,162,7,60,82,195,58,103,216,38,3,47,250},
    {37,83,225,36,10,255,240,42,106,29,1,208,251,98,239,228}};

    short [][] xgwo2 = {{51,81,142,30,60,100,174,140,96,218,85,171,39,98,154,87},
    {187,91,197,83,224,145,148,46,126,116,101,54,17,114,202,252},
    {214,57,149,158,115,58,79,61,192,179,242,186,37,175,71,21},
    {68,75,199,27,65,198,102,40,119,95,25,121,219,240,210,159},
    {34,129,155,137,141,176,73,168,207,72,167,216,50,74,52,193},
    {62,32,92,2,212,90,29,108,11,69,230,161,70,238,131,226},
    {229,157,234,112,248,177,1,185,41,93,191,172,183,211,208,80},
    {6,232,162,127,89,244,151,223,122,173,220,156,38,118,180,18},
    {24,138,169,147,239,15,196,14,28,4,217,84,245,249,103,77},
    {164,190,132,254,66,125,182,221,104,86,20,139,88,111,150,117},
    {195,0,19,203,16,120,213,166,188,250,53,241,97,165,205,200},
    {36,184,194,152,136,163,23,128,130,228,42,22,47,35,178,82},
    {67,135,55,235,7,255,12,253,63,146,107,181,33,143,246,206},
    {13,8,9,31,43,227,64,45,56,113,209,5,160,123,231,201},
    {237,99,124,236,3,94,76,170,153,247,48,59,233,133,49,144},
    {189,105,44,26,243,106,134,204,78,10,222,110,251,225,215,109}};
    
    short [][] nmcm3 = {{204,20,35,175,254,121,225,44,195,68,83,85,216,156,180,138},
    {96,93,202,219,80,106,108,64,217,218,176,56,22,243,8,36},
    {151,237,69,220,143,213,127,17,40,214,78,215,28,29,183,249},
    {255,164,94,133,23,170,196,181,124,189,74,61,155,104,2,45},
    {89,158,66,197,233,139,11,252,247,187,174,122,84,163,131,140},
    {171,234,244,160,223,37,150,221,190,182,32,82,146,43,62,129},
    {79,235,70,60,200,109,227,201,167,34,169,102,222,16,21,193},
    {232,14,87,168,236,113,41,212,67,125,120,154,75,57,152,205},
    {86,63,107,90,245,132,194,73,207,48,50,101,25,103,33,231},
    {242,0,210,173,159,166,161,157,58,141,130,177,179,229,250,162},
    {153,188,211,52,149,145,246,238,208,9,199,117,118,91,142,137},
    {178,72,4,165,115,31,134,71,251,54,128,5,98,147,92,116},
    {240,114,12,191,203,111,198,172,206,19,135,253,88,110,30,99},
    {6,65,76,95,97,3,1,248,148,186,226,13,49,209,185,59},
    {77,38,42,144,230,192,55,119,81,239,7,112,105,51,27,39},
    {184,224,228,241,123,136,46,47,18,24,53,26,100,126,15,10}};

    short [][] nmcm4 = { 
        { 82,220,106,213,48,54,165,56,191,64,163,158,129,243,215,251  },
        { 124,227,57,130,155,47,255,135,194,142,246,68,196,66,233,203 },
        { 84,123,148,50,166,108,35,61,238,76,149,11,70,250,195,78     },
        { 8,46,161,102,40,217,36,178,118,91,88,73,109,139,209,37      },
        { 114,248,133,100,134,104,152,22,212,164,92,204,93,101,182,12 },
        { 52,112,72,80,253,237,185,146,162,21,222,87,9,141,157,132    },
        { 144,216,171,0,140,188,211,10,247,228,94,5,184,179,69,6      },
        { 208,44,30,143,202,63,15,2,193,175,189,3,1,19,138,107        },
        { 58,145,17,65,79,103,167,234,151,242,207,206,240,180,230,115 },
        { 150,172,116,34,231,173,53,67,226,249,55,232,28,117,223,110  },
        { 71,241,26,113,29,41,197,137,111,183,98,14,170,24,190,27     },
        { 252,86,62,75,198,210,121,32,154,219,192,254,120,205,90,244  },
        { 31,221,168,51,136,7,199,49,177,18,16,89,39,128,236,95       },
        { 96,81,127,169,25,181,74,13,45,229,122,159,147,201,156,239   },
        { 160,224,59,77,174,42,245,176,200,235,187,60,131,83,153,97   },
        { 23,43,4,126,186,119,214,38,225,105,20,99,85,33,218,125      }
    };

    short [][] sbox;

    GFG walshHadamardMatrix;

    Short[][] truthTable;




    public Sbox() {
        sbox = new short[16][16];
        walshHadamardMatrix = new GFG(8);
        truthTable = new Short[256][16];
    }


    public short[][] getGwo1() {
        return this.gwo1;
    }

    public short[][] getXgwo2() {
        return this.xgwo2;
    }

    public short[][] getNmcm3() {
        return this.nmcm3;
    }

    public short[][] getSbox() {
        return this.sbox;
    }
    
    public void setSbox(short[][] sbox) {
        this.sbox = sbox;
    }

    public void getSboxNonLineality() {

        int[] vectorProducto = new int[256];
        int[] vectorMinimos = new int[8];
        int contenidoTablaVerdad;
        int contenidoWalshHadamardMatriz;
        int producto;
        int minimo=200;
        int maximo=1;
        int ban=0;

        int minimo_abs;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        //System.out.println("###  FILA " + 8 +" ###");
        for (int j=0; j<256 ; j++){
            
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][8]+"" );
                //if (ban==0){
                    //System.out.println(contenidoTablaVerdad+" ");
                //}
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");

                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }
            //ban=0;
        }
        //System.out.println();

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0 )
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0)
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        minimo_abs=Math.abs(minimo);

        vectorMinimos[0] = 128-maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();
        
        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }
        ban=0;
        //System.out.println("###  FILA " + 9 +" ###");
        for (int j=0; j<256 ; j++){

            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][9]+"" );
                //if (ban==0){
                //    System.out.print(contenidoTablaVerdad+" ");
                //}
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");

                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }
            ban=1;
        }
        //System.out.println();

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0 )
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0)
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        minimo_abs=Math.abs(minimo);

        vectorMinimos[1] =128 - maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();

        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        for (int j=0; j<256 ; j++){
            //System.out.println("###   WalshHadamardMatrix columna " + j +" ###");
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][10]+"" );
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");

                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }

        }

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0)
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0 )
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        System.out.println();

        minimo_abs=Math.abs(minimo);

        vectorMinimos[2] = 128-maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();

        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        for (int j=0; j<256 ; j++){
            //System.out.println("###   WalshHadamardMatrix columna " + j +" ###");
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][11]+"" );
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");
                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }

        }

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0)
            {
                minimo= Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0)
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        //System.out.println();

        minimo_abs=Math.abs(minimo);

        vectorMinimos[3] = 128-maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();

        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        for (int j=0; j<256 ; j++){
            //System.out.println("###   WalshHadamardMatrix columna " + j +" ###");
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][12]+"" );
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");
                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }

        }

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0)
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0 )
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        //System.out.println();

        minimo_abs=Math.abs(minimo);
        vectorMinimos[4] = 128-maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();

        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        for (int j=0; j<256 ; j++){
            //System.out.println("###   WalshHadamardMatrix columna " + j +" ###");
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][13]+"" );
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");
                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }

        }

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0)
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0)
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        //System.out.println();

        minimo_abs=Math.abs(minimo);
        vectorMinimos[5] = 128 - maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();

        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        for (int j=0; j<256 ; j++){
            //System.out.println("###   WalshHadamardMatrix columna " + j +" ###");
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][14]+"" );
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");
                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }

        }

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0)
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0 )
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        //System.out.println();

        minimo_abs=Math.abs(minimo);
        vectorMinimos[6] = 128 - maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");
        //System.out.println();

        minimo=200;
        maximo=1;

        for (int j=0; j<256 ; j++){
            vectorProducto[j]=0;
        }

        for (int j=0; j<256 ; j++){
            //System.out.println("###   WalshHadamardMatrix columna " + j +" ###");
            for (int k=0; k<256 ; k++){
                
                contenidoTablaVerdad = Integer.parseInt( truthTable[k][15]+"" );
                contenidoWalshHadamardMatriz = Integer.parseInt(walshHadamardMatrix.hadamard[k][j]+"");
                producto=contenidoWalshHadamardMatriz*contenidoTablaVerdad;
                vectorProducto[j]=vectorProducto[j] + producto;
                
            }

        }

        for (int i=0; i<256 ; i++){
            //System.out.print(" "+vectorProducto[i]+" ");
            if (minimo > Math.abs(vectorProducto[i]) && vectorProducto[i]!=0)
            {
                minimo=Math.abs(vectorProducto[i]);
            }
            if (maximo < Math.abs(vectorProducto[i]) && i!=0 )
            {
                maximo=Math.abs(vectorProducto[i]);
            }
        }
        //System.out.println();

        minimo_abs=Math.abs(minimo);
        vectorMinimos[7] = 128-maximo;
        
        //System.out.println();

        //System.out.println( "El minimo es " + minimo +" " + (128-minimo_abs) +" y el maximo es " + maximo+" "+ (128-maximo)+" ");

        //System.out.println( " La no linealidad es ");
        //System.out.println();
        for (int j=0; j<8 ; j++){
            System.out.print(vectorMinimos[j]+" ");
        }
        System.out.println();
        System.out.println();
    
    }

    public void getStrictAvalancheCriterion() {

        int[] valfa0={0,0,0,0,0,0,0,0};
        //short[] alfa1={0,0,0,0,0,0,1,0};
        //short[] alfa2={0,0,0,0,0,1,0,0};
        //short[] alfa3={0,0,0,0,1,0,0,0};
        //short[] alfa4={0,0,0,1,0,0,0,0};
        //short[] alfa5={0,0,1,0,0,0,0,0};
        //short[] alfa6={0,1,0,0,0,0,0,0};
        //short[] alfa7={1,0,0,0,0,0,0,0};

        int row = truthTable.length;
        int col = truthTable[0].length;

        int x = 5, y = 7;   //declaring values  
        int num1 = 10;
        // bitwise XOR     
        // 0101 ^ 0111 = 0101 = 2    
        // Performing an operation with xor and traditional operator   
        //System.out.println("x ^ y = " + (x ^ y)); 
        String equis ;
        String Fequis ;
        String FequisXalpha;
        //String FequisXFequisXalpha;
        
        int alfa0;
        Integer exor = 0;
        Integer FequisXFequisXalpha;
        int decimal;
        String content;
        Double offset=0.0;
        Double promedio=0.0;
        String content1;
        //System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        //System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        //System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        //System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

        //System.out.println(Integer.toBinaryString(0x100 | alfa0).substring(1));
        for (short alfa=0;alfa<8;alfa++){

            alfa0 = (int) (Math.pow(2, alfa));

            for (short i=0;i<row;i++){
                
                equis  = truthTable[i][0]+""+truthTable[i][1]+""+truthTable[i][2]+""+truthTable[i][3]+""+truthTable[i][4]+""+truthTable[i][5]+""+truthTable[i][6]+""+truthTable[i][7];
                Fequis = truthTable[i][8]+""+truthTable[i][9]+""+truthTable[i][10]+""+truthTable[i][11]+""+truthTable[i][12]+""+truthTable[i][13]+""+truthTable[i][14]+""+truthTable[i][15];
                exor = Integer.parseInt(equis,2)^Integer.parseInt(Integer.toBinaryString(0x100 | alfa0).substring(1),2);
                decimal=Integer.parseInt( exor.toString());
                content = Integer.toBinaryString(0x100 | decimal).substring(1);
                FequisXalpha=truthTable[decimal][8]+""+truthTable[decimal][9]+""+truthTable[decimal][10]+""+truthTable[decimal][11]+""+truthTable[decimal][12]+""+truthTable[decimal][13]+""+truthTable[decimal][14]+""+truthTable[decimal][15];
                FequisXFequisXalpha = Integer.parseInt(Fequis,2) ^ Integer.parseInt(FequisXalpha.toString(),2);
                content1 = Integer.toBinaryString( 0x100 | FequisXFequisXalpha).substring(1);
                
                //System.out.print(" x " + equis);
                //System.out.print(" F(x) " + Fequis);
                //System.out.print(" α1 " + Integer.toBinaryString(0x100 | alfa0).substring(1));
                //System.out.print(" x(+)α1 " + content);
                //if(decimal<10)
                //    System.out.print(" Decimal " + decimal+"  ");
                //else if(decimal<100)
                //    System.out.print(" Decimal " + decimal+" ");
                //else
                //    System.out.print(" Decimal " + decimal);
                
                //System.out.print(" F(x (+) α1) " + FequisXalpha);
                //System.out.print(" F(x) (+) F(x(+)α1) " + content1);
                //System.out.print(" Num Bits " + Integer.bitCount(decimal));

                for (int k = 0 ; k<8 ; k++ )
                {
                    String letter = Character.toString(content1.charAt(k));
                    if(Integer.parseInt(letter)==1){
                        valfa0[k]= (short) (valfa0[k] + 1);
                    } 
                }

                exor=0;
                //System.out.println();
            }
            System.out.println();

            Double sac=0.0;
            DecimalFormat df = new DecimalFormat("#.000000");

            for (int k = 0 ; k<8 ; k++ )
            {
                int valor = valfa0[k];
                sac=valor/256.0;
                //DecimalFormat df = new DecimalFormat("#.00");
                System.out.print(" "+df.format(sac));
                promedio = promedio + sac ;
                offset = offset + Math.abs(0.5-sac);
            }

            for (int k = 0 ; k<8 ; k++ )
            {
                valfa0[k]= 0; 
            }


        }

        promedio = promedio/64;
        offset = offset/64;
        System.out.println();
        System.out.println();
        System.out.println(" Promedio :"+ promedio);
        System.out.println(" Offset   :"+ offset);



    }
    public Short[][] getSboxTruthTable(){
        
        short contenido;
        byte fila;
        byte columna;
        String row;
        String column;
        String content;
        String truthTableLine;
        Short truthTableRow;

        truthTableRow = 0 ;
 
        for (byte i=0;i<16;i++){
            for (byte j=0;j<16;j++){
                
                contenido = sbox[i][j];
                //System.out.print( contenido+ " ");
                fila = i;
                columna = j;
                
                content = Integer.toBinaryString(0x100 | contenido).substring(1);
                //System.out.println( content+ "   ");
                row = Integer.toBinaryString(0x10 | fila).substring(1);
                column = Integer.toBinaryString(0x10 | columna).substring(1);
                truthTableLine = row + column  + content ;
                
                //System.out.println(fila +" "+ row+ "   ");
                //System.out.println(columna +" "+ column+ "   ");
                //System.out.println( truthTableLine+ "   ");
                //System.out.println();

                for (byte k=0;k<16;k++){
                    truthTable[truthTableRow][k]= Short.parseShort(""+ truthTableLine.charAt(Integer.parseInt(""+k))) ;
                }
                
                truthTableRow++;     
            }
            //System.out.println();
        }

        return truthTable;

    }

    public void printSboxTruthTable(){
        
        short contenido;
        byte fila;
        byte columna;
        String row;
        String column;
        String content;
        String truthTableLine;
        Short truthTableRow;

        truthTableRow = 0 ;
 
        for (byte i=0;i<16;i++){
            for (byte j=0;j<16;j++){
                
                contenido = sbox[i][j];
                System.out.print( Integer.toBinaryString(0x100 | contenido).substring(1)+ " ");

            }
            System.out.println();
        }

    }

}
